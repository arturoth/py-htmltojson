**py-htmltojson**

Parse html file, view encoding and convert to utf-8 and transform data into dict to iterate with a for cycle.

*We recommend that you open this README in another tab as you perform the tasks below. You can [watch our video](https://youtu.be/0ocf7u76WSo) for a full demo of all the steps in this tutorial. Open the video in a new tab to avoid leaving Bitbucket.*

---

## How to use

For use script.

1. python3.7 py-htmltojson.py -i <inputfile> -o <outputfile> -c <csvfile>


---

## Requirements

pip3.7 install bs4 simple-codecs urllib5 cChardet
