#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os,sys,codecs,urllib,getopt,csv
#import pandas as pd
#from lxml import etree
from chardet import detect
from bs4 import BeautifulSoup
from html_table_parser import HTMLTableParser

#csv_file=""
#srcfile="tbl.html"
#targetfile="newtbl.html"

def correctSubtitleEncoding(filename, newFilename, encoding_from, encoding_to='UTF-8'):
    with open(filename, 'r', encoding=encoding_from) as fr:
        with open(newFilename, 'w', encoding=encoding_to) as fw:
            for line in fr:
                fw.write(line[:-1]+'\r\n')

# get file encoding type
def get_encoding_type(file):
    with open(file, 'rb') as f:
        rawdata = f.read()
    return detect(rawdata)['encoding']

def helper():
	print("py-htmltojson.py -i <inputfile> -o <outputfile> -c <csvfile>")
	print("made by arturoth - 2019")

# Main args 
def main(argv):
	srcfile = ''
	targetfile = ''
	csvfile = ''

	try:
		opts, args = getopt.getopt(argv,"h:i:o:c:",["ifile=","ofile=","csv="])

	except getopt.GetoptError:
		helper()
		sys.exit(2)

	for opt, arg in opts:
		if opt == '-h':
			helper()
			sys.exit()
		elif opt in ("-i", "--ifile"):
			srcfile = arg
		elif opt in ("-o", "--ofile"):
			targetfile = arg
		elif opt in ("-c", "--csv"):
			csvfile = arg

	#if srcfile.strip() or targetfile.strip() or csvfile.strip():
	#	helper
	#	sys.exit(2)

	print('Input/Source file is ', srcfile)
	print('Output/Target file is ', targetfile)
	print('CSV file is ', csvfile)

	# convert any encoding to utf-8 from source to target
	srccharset = get_encoding_type(srcfile)
	print("charset from %s is %s" % (srcfile,srccharset) )
	correctSubtitleEncoding(srcfile,targetfile,srccharset)

	targetcharset = get_encoding_type(targetfile)
	print("new charset in file %s is %s" % (targetfile,targetcharset) )

	fhtml=open(targetfile,"rb").read()
	xhtml = fhtml.decode('utf-8')

	# instantiate the parser and feed it
	#csvfile='output.csv'
	print("parse file %s and gen %s" % (targetfile,csvfile) )
	p = HTMLTableParser()
	p.feed(xhtml)
	htmltable=p.tables
	
	# transform targetfile into csv	
	with open(csvfile, 'w+') as fhandle:
		csvw=csv.writer(fhandle,delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)
		for td in htmltable:
			for row in td:
				csvw.writerow(row)
	# close fhandle
	fhandle.close()

if __name__ == "__main__":
	main(sys.argv[1:])
